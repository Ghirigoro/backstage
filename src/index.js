const { windowManager } = require("node-window-manager");
const { execFile } = require("child_process");
const { readFileSync, existsSync } = require("fs");
const { basename, resolve, dirname } = require("path");
const readline = require("readline");
const Connection = require("./connection");

////////////////////////////////////////////////////////////////
// UTILITY METHODS
////////////////////////////////////////////////////////////////
const exists = path => {
    try {
        existsSync(path);
        return true;
    } catch (e) {
        return false;
    }
};

const isWindowOfTrackedApp = (trackedAppPaths, window) => {
    const windowAppName = basename(window.path);
    for (let i = 0; i < trackedAppPaths.length; i++) {
        const trackedAppName = basename(trackedAppPaths[i]);
        if (trackedAppName === windowAppName) return true;
    }
};

////////////////////////////////////////////////////////////////
// COMMAND LINE PARAMETERS
////////////////////////////////////////////////////////////////
const program = require("commander");
program.version("0.1.0");
program.requiredOption("-c, --config <path>", "path to the config file");

program.parse(process.argv);

let config;

if (!exists(program.config)) throw Error("Could not find config file");

console.log("Loading config: " + program.config);

try {
    config = JSON.parse(readFileSync(program.config).toString());
    console.log(config);
    const configDir = dirname(program.config);
    if (config.x === undefined) throw Error("Missing x in config");
    if (config.y === undefined) throw Error("Missing y in config");
    if (config.apps === undefined) throw Error("Missing apps in config");
    if (!Array.isArray(config.apps))
        throw Error("Config apps needs to be an array");
    if (config.apps.length < 2)
        throw Error("Need to specify at least 2 apps in config");
    config.apps = config.apps.map(p => resolve(configDir, p));
    config.apps.forEach(p => {
        if (!exists(p)) throw Error("Could not find app: " + p);
    });
} catch (e) {
    throw Error("Could not load config: " + e.message);
}

////////////////////////////////////////////////////////////////
// WINDOW MANAGER
////////////////////////////////////////////////////////////////
windowManager.on("window-activated", window => {
    // TODO: Check that the window is of a type that is being managed
    if (window.isWindow() && isWindowOfTrackedApp(config.apps, window)) {
        console.log("ACTIVATING: " + basename(window.path));
        window.setBounds({
            x: config.x,
            y: config.y
        });
        window.bringToTop();
    }
});

////////////////////////////////////////////////////////////////
// LOGIC
////////////////////////////////////////////////////////////////
const activeApps = []; // list of processes

const openApp = path => {
    // close all apps except the background
    for (let i = 1; i < activeApps.length; i++) {
        activeApps[i].kill();
    }

    const f = execFile(path, err => {
        const i = activeApps.indexOf(f);
        if (i !== -1) activeApps.splice(i, 1);
    });
    activeApps.push(f);
};

const init = () => {
    // open the background app
    openApp(config.apps[0]);

    readline.emitKeypressEvents(process.stdin);
    process.stdin.setRawMode(true);
    process.stdin.on("keypress", (str, key) => {
        if (key.ctrl && key.name === "c") process.exit();
        const n = parseInt(str);
        if (n !== undefined && n >= 0 && n < config.apps.length) {
            openApp(config.apps[n]);
        }
    });

    setInterval(() => {
        if (activeApps.length === 0) {
            process.exit();
        }
    }, 100);
};

init();

////////////////////////////////////////////////////////////////
// CONNECTION
////////////////////////////////////////////////////////////////
const url = "https://www.bigscreens.live/";
const room = "Backstage";
const type = "admin";

const connection = new Connection(room, type, url);

connection.onConnect(() => {
    console.log("Connected");
});

connection.onDisconnect(() => {
    console.log("Disconnected");
});

connection.on("open", (sender, index) => {
    const path = config.apps[index];
    if (path !== undefined) openApp(path);
    else console.log(sender, "requested unknown index:", index);
});

connection.on("quit", () => {
    process.exit();
});
